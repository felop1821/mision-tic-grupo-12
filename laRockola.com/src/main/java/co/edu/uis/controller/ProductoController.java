package co.edu.uis.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import co.edu.uis.model.Producto;
import co.edu.uis.repository.ProductoRepository;

@Controller
@RequestMapping("/listadepista")//http://localhost:8080/listadepista
public class ProductoController {
	
	@Autowired
	private ProductoRepository productorepository;
	
	@GetMapping("")
	public String home(Model model) {
		model.addAttribute("listadepista", productorepository.findAll());
		return "home";
	}
	@GetMapping("/create")
	public String create() {
		return"create";
	}
	@PostMapping("/save")
	public String save(Producto producto) {
		//System.out.println(producto);
		productorepository.save(producto);
		return"redirect:/listadepista";
	}
	
	@GetMapping("/edit/{id}") 
	public String edit(@PathVariable Integer id, Model model) {
		//Producto p = productorepository.getOne(id);
		Producto p = productorepository.findById(id).orElse(null);
		System.out.println(p);
		model.addAttribute("producto",p);
		return"edit";
	}
	
	@GetMapping("/delete/{id}")
	public String delete(@PathVariable Integer id) {
		//Producto p = productorepository.getOne(id);
		Producto p = productorepository.findById(id).orElse(null);
		System.out.println(p);
		productorepository.delete(p); 
		return"redirect:/listadepista";
	}

}
