package co.edu.uis.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="listadepista")
public class Producto {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="idlistadepista")
	private Integer idListadePista;
	@Column(name="nombrepista")
	private String nombrePista;
	@Column(name="genero")
	private String genero;
	@Column(name="duracion")
	private String duracion;
	@Column(name="albumidalbum")
	private String AlbumidAlbum;
	@Column(name="artistaidartista")
	private String ArtistaidArtista;
	public Producto(Integer idListadePista, String nombrePista, String genero, String duracion, String albumidAlbum,
			String artistaidArtista) {
		super();
		this.idListadePista = idListadePista;
		this.nombrePista = nombrePista;
		this.genero = genero;
		this.duracion = duracion;
		AlbumidAlbum = albumidAlbum;
		ArtistaidArtista = artistaidArtista;
	}
	public Producto() {
		super();
	}
	public Integer getIdListadePista() {
		return idListadePista;
	}
	public void setIdListadePista(Integer idListadePista) {
		this.idListadePista = idListadePista;
	}
	public String getNombrePista() {
		return nombrePista;
	}
	public void setNombrePista(String nombrePista) {
		this.nombrePista = nombrePista;
	}
	public String getGenero() {
		return genero;
	}
	public void setGenero(String genero) {
		this.genero = genero;
	}
	public String getDuracion() {
		return duracion;
	}
	public void setDuracion(String duracion) {
		this.duracion = duracion;
	}
	public String getAlbumidAlbum() {
		return AlbumidAlbum;
	}
	public void setAlbumidAlbum(String albumidAlbum) {
		AlbumidAlbum = albumidAlbum;
	}
	public String getArtistaidArtista() {
		return ArtistaidArtista;
	}
	public void setArtistaidArtista(String artistaidArtista) {
		ArtistaidArtista = artistaidArtista;
	}
	@Override
	public String toString() {
		return "Producto [idListadePista=" + idListadePista + ", nombrePista=" + nombrePista + ", genero=" + genero
				+ ", duracion=" + duracion + ", AlbumidAlbum=" + AlbumidAlbum + ", ArtistaidArtista=" + ArtistaidArtista
				+ "]";
	}
	
	
	
	}
