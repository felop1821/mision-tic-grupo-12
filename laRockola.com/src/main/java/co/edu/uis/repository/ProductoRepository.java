package co.edu.uis.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import co.edu.uis.model.Producto;

@Repository
public interface ProductoRepository extends JpaRepository<Producto, Integer>{

}
